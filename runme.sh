#export JAVA_HOME=../jdk1.8.0_77

TRAIN_DIR=data/train
TEST_DIR=data/test
KMEANS_RESULT_DIR=result-kmeans
GMIXTURE_RESULT_DIR=result-gmixture
BKMEANS_RESULT_DIR=result-bisectingkmeans
COLS="ChamberPressureManometer_AI,ChuckESCPreviousChargingTime"

echo "***"
echo "*** Compiling and running tests  **"
echo "***"

rm -r -f $KMEANS_RESULT_DIR $GMIXTURE_RESULT_DIR $BKMEANS_RESULT_DIR &&
../apache-maven-3.3.9/bin/mvn package &&
spark-1.6.0-bin-hadoop2.6/bin/spark-submit --class TrainAndPredict --master local[40] target/mllib-project-1.0.jar KMeans $TRAIN_DIR $TEST_DIR $KMEANS_RESULT_DIR $COLS 50 50 2>&1 &&
# spark-1.6.0-bin-hadoop2.6/bin/spark-submit --class TrainAndPredict --master local[40] target/mllib-# project-1.0.jar GaussianMixture $TRAIN_DIR $TEST_DIR $GMIXTURE_RESULT_DIR $COLS 50 2>&1 &&
# spark-1.6.0-bin-hadoop2.6/bin/spark-submit --class TrainAndPredict --master local[40] target/mllib-# project-1.0.jar BisectingKMeans $TRAIN_DIR $TEST_DIR $BKMEANS_RESULT_DIR $COLS 50 2>&1

echo "***"
echo "*** Done (status=$?) ***"
echo "*** Cleaning up ***"
echo "***"

[[ $? -eq 0 ]] || exit 1

../apache-maven-3.3.9/bin/mvn clean
