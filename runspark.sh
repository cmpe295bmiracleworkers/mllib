cd /var/lib/jenkins/Desktop/puja/mllib
export JAVA_HOME=../jdk1.8.0_77

if [ $# -lt 6 ]; then
	echo "Usage: runspark.sh <algo> <train directory> <test directory> <result-directory> <waferid> <cols>"
	exit 1
fi

ALGO=$1
TRAIN_DIR=$2
TEST_DIR=$3
RESULT_DIR=$4
WAFERID=$5
COLS=$6

rm -r -f $RESULT_DIR &&
# ../apache-maven-3.3.9/bin/mvn package &&
spark-1.6.0-bin-hadoop2.6/bin/spark-submit --class TrainAndPredict --master local[40] target/mllib-project-1.0.jar $ALGO $TRAIN_DIR $TEST_DIR $RESULT_DIR $WAFERID $COLS 2>&1

cp -r $RESULT_DIR/$ALGO/prediction-readable /var/lib/jenkins/Desktop/logFiles/sparkOutput.txt
