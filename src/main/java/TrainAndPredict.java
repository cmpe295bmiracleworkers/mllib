import java.util.regex.Pattern;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.clustering.BisectingKMeans;
import org.apache.spark.mllib.clustering.BisectingKMeansModel;
import org.apache.spark.mllib.clustering.GaussianMixture;
import org.apache.spark.mllib.clustering.GaussianMixtureModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.classification.*;

import org.apache.spark.rdd.RDD;

import java.util.*;
import java.lang.System.*;
import java.io.*;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.commons.io.IOUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.regression.LinearRegressionModel;
import org.apache.spark.mllib.regression.LinearRegressionWithSGD;

import org.json.*;

// import org.apache.spark.api.java.JavaPairRDD;
// import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;

public final class TrainAndPredict {
	private static class Es {
		public static int GetIndex(String index, String wid, List<String> lines) throws Exception {
			int from = 0;
			final int pagination_size = 1000;
			final int max_lines = 10000;
			while (true) {
			String url = "http://localhost:9200/" + index + "/_search?q=*&size=" + 
				     pagination_size + "&from=" + from;
			HttpClient client = HttpClientBuilder.create().build();
 			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			String data = IOUtils.toString(response.getEntity().getContent(), "utf-8");
			
			System.out.println(lines.size() + " lines matched for wafer id");
			System.out.println(request + "=>" + response);
			if (response.getStatusLine().getStatusCode() != 200) {
				return response.getStatusLine().getStatusCode();
			}

			from += pagination_size;

			JSONObject jobj = new JSONObject(data);
			JSONArray arr = jobj.getJSONObject("hits").getJSONArray("hits");
			if (arr.length() == 0 || lines.size() > max_lines) {
				// end of pagination
				return response.getStatusLine().getStatusCode();
			}
			for (int i = 0; i < arr.length(); i++) {
				JSONObject obj = arr.getJSONObject(i).getJSONObject("_source");
				String waferid = obj.getString("waferId");
				if (wid.isEmpty() || waferid.equals(wid)) {
					lines.add(obj.toString());
				} else {
					//System.out.println("wafer id mismatch. " + waferid);
				}		
			}

			// force gc
			System.gc();
			}

			// never reached
		}

		public static JavaRDD<String> GetLines(JavaSparkContext sc, final String index, final String wid) throws Exception {
			List<String> lines = new ArrayList<String>();
			if (GetIndex(index, wid, lines) == 200) {
				return sc.parallelize(lines);
			}
			throw new Exception("Error reading JSON");
		}

		public static void CreateIndex(final String index) throws Exception {
			String url = "http://localhost:9200/" + index;
			HttpClient httpClient = HttpClientBuilder.create().build();
            		HttpPut putRequest = new HttpPut(url);
            		putRequest.addHeader("Content-Type", "application/json");
            		putRequest.addHeader("Accept", "application/json");
            		HttpResponse response = httpClient.execute(putRequest);
			// System.out.println(putRequest + " ==> " + response);
			int status = response.getStatusLine().getStatusCode();  
           		if (status != 200 && status != 201) {
				throw new Exception("Error adding index " + index + ". HTTP code " + status);
			}
		}

		public static void DeleteIndex(final String index) throws Exception {
			String url = "http://localhost:9200/" + index;
			HttpClient httpClient = HttpClientBuilder.create().build();
            		HttpDelete putRequest = new HttpDelete(url);
            		putRequest.addHeader("Content-Type", "application/json");
            		putRequest.addHeader("Accept", "application/json");
            		HttpResponse response = httpClient.execute(putRequest);
			// System.out.println(putRequest + " ==> " + response);
			int status = response.getStatusLine().getStatusCode();  
			if (status != 200 && status != 404) {
				throw new Exception("Error deleting index " + index + ". HTTP code " + status);
			}
		}

		public static void AddResult(final String index, final int id, final String input, final String output) throws Exception {
			String url = "http://localhost:9200/" + index + "/" + id;
			HttpClient httpClient = HttpClientBuilder.create().build();
            		HttpPut putRequest = new HttpPut(url);
            		putRequest.addHeader("Content-Type", "application/json");
            		putRequest.addHeader("Accept", "application/json");
            		JSONObject keyArg = new JSONObject();
            		keyArg.put("test-value", input);
            		keyArg.put("predicted-value", output);
            		StringEntity data = new StringEntity(keyArg.toString());
            		putRequest.setEntity(data);
            		HttpResponse response = httpClient.execute(putRequest);
			// System.out.println(putRequest + " ==> " + response);
           		int status = response.getStatusLine().getStatusCode();
			if (status != 200 && status != 201) {
				throw new Exception("Error adding result to index " + index + ". HTTP code " + status);
			}
  		}
 	}
 
	private static class ParseLabeledPoint implements Function<String, LabeledPoint> {
      		private static final Pattern COMMA = Pattern.compile(",");
      		private static final Pattern COLON = Pattern.compile(":");
      
      		private String[] columns_;
      
      		public ParseLabeledPoint(final String columns) {
          		this.columns_ = COMMA.split(columns);
      		}
      
      		@Override
      		public LabeledPoint call(String line) {
          		String debug = "DEBUG ";
          		line = line.replace("{", "");
          		line = line.replace("}", "");
          		String[] tok = COMMA.split(line);
			boolean labelfilled = false;
			double label = 0.0;
          		double[] point = new double[columns_.length - 1];
          		int pos = 0;
          		for (int i = 0; i < tok.length; ++i) {
              			String[] kv = COLON.split(tok[i]);
              			for (int j = 0; j < columns_.length; j++) {
                  			if (kv[0].equals("\"" + columns_[j] + "\"")) {
                      				debug += columns_[j] + "=>" + kv[1] + " ";
                      				try {
							if (!labelfilled) {
								label = Double.parseDouble(kv[1]);
								labelfilled = true;
							} else {
                          					point[pos] = Double.parseDouble(kv[1]);
								pos++;
							}
                      				} catch (Exception e) {
                          				System.err.println(e);
							if (!labelfilled) {
								label = 0;
								labelfilled = true;
							} else {
                          					point[pos] = 0;
								pos++;
							}
                      				}
                      				break;
                  			}
              			}
          		}

          		debug += " [ " + label + " ";
          		for (double p : point) {
              			debug += p + " ";
          		}
          		debug += "]";
          		// System.out.println(new LabeledPoint(label, Vectors.dense(point)));
          		return new LabeledPoint(label, Vectors.dense(point));
      		}
    	}


  private static class ParsePoint implements Function<String, Vector> {
      private static final Pattern COMMA = Pattern.compile(",");
      private static final Pattern COLON = Pattern.compile(":");
      
      private String[] columns_;
      
      public ParsePoint(final String columns) {
          this.columns_ = COMMA.split(columns);
      }
      
      @Override
      public Vector call(String line) {
          String debug = "DEBUG ";
          line = line.replace("{", "");
          line = line.replace("}", "");
          String[] tok = COMMA.split(line);
          double[] point = new double[columns_.length];
          int pos = 0;
          Random rnd = new Random();
          for (int i = 0; i < tok.length; ++i) {
              String[] kv = COLON.split(tok[i]);
              for (int j = 0; j < columns_.length; j++) {
                  if (kv[0].equals("\"" + columns_[j] + "\"")) {
                      debug += columns_[j] + "=>" + kv[1] + " ";
                      try {
                          point[pos] = Double.parseDouble(kv[1]) * 100 + rnd.nextInt(10);
                      } catch (Exception e) {
                          System.err.println(e);
                          point[pos] = 0;
                      }
                      pos++;
                      break;
                  }
              }
          }

          debug += " [";
          for (double p : point) {
              debug += p + " ";
          }
          debug += "]";
          // System.out.println(debug);
          return Vectors.dense(point);
      }
    }

    public List<File> GetFiles(final String folderName) {
        File folder = new File(folderName);
        File[] files = folder.listFiles();
        
        List<File> result = new ArrayList<File>();
        for (File f : files) {
            if (f.isFile()) {
                System.out.println("==>" + f.getAbsolutePath());
                result.add(f);
            }
        }
        return result;
    }
    
    public JavaRDD<String> GetLines(JavaSparkContext sc, List<File> files) {
        JavaRDD<String> result = sc.emptyRDD();
        for (File f : files) {
            JavaRDD<String> lines = sc.textFile(f.getAbsolutePath());
            result = result.union(lines);
        }
        return result;
    }
    
    private boolean zeros(Vector v) {
        double[] arr = v.toArray();
	for (double a : arr) {
		if (a != 0) return false;
	}
	return true;
    }

    public void KMeans(final String trainSource, final String predictSource, final String resultSource, final String wid, final String columns, int k, int iterations) throws Exception {
        int runs = 50;
        
        SparkConf sparkConf = new SparkConf().setAppName("JavaKMeans");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
	JavaRDD<String> lines = Es.GetLines(sc, trainSource, "");
        JavaRDD<Vector> points = lines.map(new ParsePoint(columns));

        KMeansModel model = KMeans.train(points.rdd(), k, iterations, runs);
        
        System.out.println("Cluster centers:");
        for (Vector center : model.clusterCenters()) {
            System.out.println(" " + center);
        }
        double cost = model.computeCost(points.rdd());
        System.out.println("Cost: " + cost);
        
        sc.stop();
        
        sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> p_lines = Es.GetLines(sc, predictSource, wid);
        JavaRDD<Vector> p_points = p_lines.map(new ParsePoint(columns));
        JavaRDD<Integer> predictions = model.predict(p_points);
        predictions.rdd().saveAsTextFile(resultSource + "/KMeans/prediction-spark");
        
	PrintWriter writer = new PrintWriter(resultSource + "/KMeans/prediction-readable", "UTF-8");

	Es.DeleteIndex(resultSource);
	Es.CreateIndex(resultSource);
        List<Vector> pp = p_points.collect();
	int i = 0;
	writer.print("[");
        for (Vector p : pp) {
		int index = model.predict(p);
		if (i != 0) writer.print(",");
            	writer.print(p);
        	Es.AddResult(resultSource + "/kmeans", i++, "" + p, "" + model.clusterCenters()[index]);
	}
	i = 0;
	writer.println("]\n");
	writer.print("[");
	for (Vector p : pp) {
		int index = model.predict(p);	
		if (i != 0) writer.print(",");
		i++;
            	writer.print(model.clusterCenters()[index]);
	}
	writer.println("]");
        writer.close();
        
        sc.stop();
    }

    public void BisectingKMeans(final String trainSource, final String predictSource, final String resultSource, final String wid, final String columns, int k) throws Exception {
        int runs = 10;
        
        SparkConf sparkConf = new SparkConf().setAppName("JavaKMeans");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
       	JavaRDD<String> lines = Es.GetLines(sc, trainSource, "");
        JavaRDD<Vector> points = lines.map(new ParsePoint(columns));
        
        BisectingKMeansModel model = new BisectingKMeans().setK(k).run(points.rdd());
        
        System.out.println("Cluster centers:");
        for (Vector center : model.clusterCenters()) {
            System.out.println(" " + center);
        }
        double cost = model.computeCost(points.rdd());
        System.out.println("Cost: " + cost);
        
        sc.stop();
        
        sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> p_lines = Es.GetLines(sc, predictSource, wid);
        JavaRDD<Vector> p_points = p_lines.map(new ParsePoint(columns));
        JavaRDD<Integer> predictions = model.predict(p_points);
        predictions.rdd().saveAsTextFile(resultSource + "/BisectingKMeans/prediction-spark");
        
	PrintWriter writer = new PrintWriter(resultSource + "/BisectingKMeans/prediction-readable", "UTF-8");
        
	Es.DeleteIndex(resultSource);
	Es.CreateIndex(resultSource);
        List<Vector> pp = p_points.collect();
	int i = 0;
	writer.print("[");
        for (Vector p : pp) {
            int index = model.predict(p);
	    if (i != 0) writer.print(",");
	    writer.print(p);
            Es.AddResult(resultSource + "/bisectingkmeans", i++, "" + p, "" + model.clusterCenters()[index]);
	}
	writer.println("]\n");
	writer.print("[");
	i = 0;
  	for (Vector p : pp) {
            int index = model.predict(p);
	    if (i != 0) writer.print(",");
	    i++;
            writer.print(model.clusterCenters()[index]);
	}
	writer.println("]");
        writer.close();
        
        sc.stop();
    }

    public void GaussianMixture(final String trainSource, final String predictSource, final String resultSource, final String wid, final String columns, int k) throws Exception {
        SparkConf sparkConf = new SparkConf().setAppName("JavaKMeans");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> lines = Es.GetLines(sc, trainSource, "");
        JavaRDD<Vector> points = lines.map(new ParsePoint(columns));
        
        GaussianMixtureModel model = new GaussianMixture().setK(k).run(points.rdd());
        
        // Output the parameters of the mixture model
        for(int j = 0; j < model.k(); j++) {
            System.out.printf("weight=%f\nmu=%s\nsigma=\n%s\n",
                              model.weights()[j], model.gaussians()[j].mu(), model.gaussians()[j].sigma());
        }
        
        sc.stop();
        
        sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> p_lines = Es.GetLines(sc, predictSource, wid);
        JavaRDD<Vector> p_points = p_lines.map(new ParsePoint(columns));
        JavaRDD<Integer> predictions = model.predict(p_points);
        predictions.rdd().saveAsTextFile(resultSource + "/GaussianMixture/prediction-spark");
        
	PrintWriter writer = new PrintWriter(resultSource + "/GaussianMixture/prediction-readable", "UTF-8");
        
	Es.DeleteIndex(resultSource);
	Es.CreateIndex(resultSource);
	List<Vector> pp = p_points.collect();
	int i = 0;
	writer.print("[");
        for (Vector p : pp) {
            	int index = model.predict(p);
		if (i != 0) writer.print(",");
            	writer.println(p);
        	Es.AddResult(resultSource + "/gaussianmixture", i++, "" + p, "" + model.gaussians()[index].mu());
	}
	i = 0;
	writer.println("]\n");
	writer.print("[");
        for (Vector p : pp) {
            	int index = model.predict(p);
		if (i != 0) writer.print(",");
            	i++;
		writer.print(model.gaussians()[index].mu());
	}
 	writer.println("]");
        writer.close();
        sc.stop();
    }

    private String ToString(LabeledPoint p) {
    	String str = new String();
	str += "[" + p.label() + ",";
	int i = 0;
	for (double d : p.features().toArray()) {
		if (i++ != 0) str += ",";
		str += d;
	}
	str += "]";
	return str;
    }      

    private String ToString(double label, LabeledPoint p) {
    	String str = new String();
	str += "[" + (label + p.label()) + ",";
	System.out.println(label + "+" + p.label() + "=" + (label + p.label()));
	int i = 0;
	for (double d : p.features().toArray()) {
		if (i++ != 0) str += ",";
		str += d;
	}
	str += "]";
	return str;
    }      

    private static double[] compute(List<LabeledPoint> pp) {
     	int MAXN = 1000;
        int n = 0;

        // first pass: read in data, compute xbar and ybar
        double sumx = 0.0, sumy = 0.0, sumx2 = 0.0;
        for (LabeledPoint p : pp) {
            double x = p.label();
            double y = p.features().toArray()[0];
            sumx  += x;
            sumx2 += x * x;
            sumy  += y;
            n++;
        } 
        double xbar = sumx / n;
        double ybar = sumy / n;

        // second pass: compute summary statistics
        double xxbar = 0.0, yybar = 0.0, xybar = 0.0;
        for (LabeledPoint p : pp) {
	    double x = p.label();
            double y = p.features().toArray()[0];
            xxbar += (x - xbar) * (x - xbar);
            yybar += (y - ybar) * (y - ybar);
            xybar += (x - xbar) * (y - ybar);
        }
        double beta1 = xybar / xxbar;
        double beta0 = ybar - beta1 * xbar;

        // print results
        System.out.println("---> y   = " + beta1 + " * x + " + beta0);

	double ret[] = {beta1, beta0};
	return ret;
    }

    private double predict(double[] rline, double x, double y) {
       double y_calc = x * rline[0] + rline[1];
       System.out.println(x + " => " + y_calc + " / " + y);
       return y_calc;
    }

    public void LinearRegression(final String trainSource, final String predictSource, final String resultSource, final String wid, final String columns) throws Exception {
        SparkConf sparkConf = new SparkConf().setAppName("LinearRegression");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
	JavaRDD<String> lines = Es.GetLines(sc, trainSource, "");
        JavaRDD<LabeledPoint> points = lines.map(new ParseLabeledPoint(columns));	
	points.cache();
	for (LabeledPoint p : points.collect()) {
		// System.out.println("=>" + p);
	}

	double[] rline = compute(points.collect());
/*	
	int numIterations = 100;
    	double stepSize = 0.00000001;
    	final LinearRegressionModel model =
      	LinearRegressionWithSGD.train(JavaRDD.toRDD(points), numIterations, stepSize);
*/
        sc.stop();


        sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> p_lines = Es.GetLines(sc, predictSource, wid);
        JavaRDD<LabeledPoint> p_points = p_lines.map(new ParseLabeledPoint(columns));

	final String outdir = resultSource + "/LinearRegression"; 
	File f = new File(outdir);
	f.mkdirs();

	PrintWriter writer = new PrintWriter(outdir + "/prediction-readable", "UTF-8");

	Es.DeleteIndex(resultSource);
	Es.CreateIndex(resultSource);
        List<LabeledPoint> pp = p_points.collect();
	int i = 0;
        writer.print("regression\n");
	writer.print("[");
	for (LabeledPoint p : pp) {
		if (i++ != 0) writer.print(",");
		writer.print(ToString(p));
	}
	writer.println("]\n");
	i = 0;
	writer.print("[");
        for (LabeledPoint p : pp) {
            	// double label = model.predict(p.features());
		if (i++ != 0) writer.print(",");
        	//writer.print(ToString(label, p));
		double label = p.label();
		double feature = predict(rline, label, p.features().toArray()[0]);
		writer.print("[" + label + "," + feature +"]");
		// Es.AddResult(resultSource + "/LinearRegression", i, ToString(p), ToString(label, p));
	}
	writer.println("]");
        writer.close();
        
        sc.stop();
    }

    public void SVM(final String trainSource, final String predictSource, final String resultSource, final String wid, final String columns) throws Exception {
        SparkConf sparkConf = new SparkConf().setAppName("LinearRegression");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
	JavaRDD<String> lines = Es.GetLines(sc, trainSource, "");
        JavaRDD<LabeledPoint> points = lines.map(new ParseLabeledPoint(columns));
	points.cache();
   
        // Run training algorithm to build the model.
        final LogisticRegressionModel model = new LogisticRegressionWithLBFGS()
      								.setNumClasses(1000)
      								.run(points.rdd());
        sc.stop();

        sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> p_lines = Es.GetLines(sc, predictSource, wid);
        JavaRDD<LabeledPoint> p_points = p_lines.map(new ParseLabeledPoint(columns));

	final String outdir = resultSource + "/SVM"; 
	File f = new File(outdir);
	f.mkdirs();

	PrintWriter writer = new PrintWriter(outdir + "/prediction-readable", "UTF-8");

	Es.DeleteIndex(resultSource);
	Es.CreateIndex(resultSource);
        List<LabeledPoint> pp = p_points.collect();
	int i = 0;
	writer.print("[");
	for (LabeledPoint p : pp) {
		if (i++ != 0) writer.print(",");
		writer.print(ToString(p));
	}
	writer.println("]\n");
	i = 0;
	writer.print("[");
        for (LabeledPoint p : pp) {
            	double label = model.predict(p.features());
		if (i != 0) writer.print(",");
        	Es.AddResult(resultSource + "/SVM", i++, ToString(p), ToString(label, p));
		writer.print(ToString(label, p));
	}
	writer.println("]");
        writer.close();
        
        sc.stop();
    }

    public static void PrintUsage(final String error) {
        if (!error.isEmpty()) {
            System.err.println("ERROR: " + error);
        }
            
        System.err.println("<algo> <train index> <predict index> <result index> <waferid> <colums>");
        System.err.println("Algo: KMeans/GaussianMixture/BisectingKMeans/LinearRegression/SVM");
    }
  
    public static void main(String[] args) throws Exception {
        if (args.length < 6) {
            PrintUsage("Invalid args");
            System.exit(1);
        }

        String algo = args[0];
        String inputDirectory = args[1];
        String predictDirectory = args[2];
        String resultDirectory = args[3];
	String waferid = args[4];
        String columns = args[5];

        TrainAndPredict tp = new TrainAndPredict();
        if (algo.equals("KMeans")) {
            int k = 3;
            int iterations = 50;
            tp.KMeans(inputDirectory, predictDirectory, resultDirectory, waferid, columns, k, iterations);
        } else if (algo.equals("GuassianMixture")) {
            int k = 50;
            tp.GaussianMixture(inputDirectory, predictDirectory, resultDirectory, waferid, columns, k);
        } else if (algo.equals("BisectingKMeans")) {
            int k = 50;
            tp.BisectingKMeans(inputDirectory, predictDirectory, resultDirectory, waferid, columns, k);
        } else if (algo.equals("LinearRegression")) {
	    tp.LinearRegression(inputDirectory, predictDirectory, resultDirectory, waferid, columns);
	} else if (algo.equals("SVM")) {
	    tp.SVM(inputDirectory, predictDirectory, resultDirectory, waferid, columns);
	} else {
            PrintUsage("Invalid algo");
            System.exit(1);
        }
    }
}
